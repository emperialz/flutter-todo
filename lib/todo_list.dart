import 'package:flutter/material.dart';
import 'package:todo/todo.dart';

typedef ToggleTodoCallback = void Function(Todo, bool);

class TodoList extends StatelessWidget {
  TodoList({@required this.todos, this.onTodoToggle});

  final List<Todo> todos;
  final ToggleTodoCallback onTodoToggle;

  Widget _buildItem(BuildContext context, int index) {
    final todo = todos[index];
    return CheckboxListTile(
      value: todo.isDone,
      title: Text(todo.title,
          style: todo.isDone == false
              ? TextStyle(decoration: null)
              : TextStyle(
                  decoration: TextDecoration.lineThrough, color: Colors.grey)),
      onChanged: (bool isChecked) {
        onTodoToggle(todo, isChecked);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemBuilder: _buildItem,
      itemCount: todos.length,
      separatorBuilder: (context, index) {
        return Divider();
      },
    );
  }
}
