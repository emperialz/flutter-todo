import 'package:flutter/material.dart';
import 'package:todo/todo.dart';
import 'package:todo/todo_dialog.dart';
import 'package:todo/todo_list.dart';

class TodoListScreen extends StatefulWidget {
  @override
  _TodoListScreenState createState() => _TodoListScreenState();
}

class _TodoListScreenState extends State<TodoListScreen> {
  final TextEditingController _filter = new TextEditingController();

  List<Todo> todos = [];
  String _searchText = "";
  List<Todo> filteredTodos = [];

  SortingManager sm = new SortingManager();

  Icon _searchIcon = new Icon(Icons.search);
  Widget _appBarTitle = new Text('Todo list');

  _TodoListScreenState() {
    _filter.addListener(() {
      setState(() {
        _searchText = _filter.text;
      });
    });
  }

  _toggleTodo(Todo todo, bool isChecked) {
    setState(() {
      todo.isDone = isChecked;
    });
    todos = sm.sortTodos(todos);
  }

  _addTodo() async {
    final todo = await showDialog<Todo>(
      context: context,
      builder: (BuildContext context) {
        return NewTodoDialog();
      },
    );

    if (todo != null) {
      setState(() {
        todos.add(todo);
      });
    }
    todos = sm.sortTodos(todos);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildBar(context),
      body: TodoList(
        todos: sm.searchFilter(_searchText, todos),
        onTodoToggle: _toggleTodo,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: _addTodo,
      ),
    );
  }

  Widget _buildBar(BuildContext context) {
    return new AppBar(
      centerTitle: true,
      title: _appBarTitle,
      leading: new IconButton(
        icon: _searchIcon,
        onPressed: _searchPressed,
      ),
    );
  }

  void _searchPressed() {
    setState(() {
      if (this._searchIcon.icon == Icons.search) {
        this._searchIcon = new Icon(Icons.close);
        this._appBarTitle = new TextField(
          controller: _filter,
          decoration: new InputDecoration(
              prefixIcon: new Icon(Icons.search), hintText: 'Search...'),
        );
      } else {
        this._searchIcon = new Icon(Icons.search);
        this._appBarTitle = new Text('Todo list');
        filteredTodos = todos;
        _filter.clear();
      }
    });
  }
}

class SortingManager {
  List<Todo> searchFilter(String searchText, List<Todo> todos) {
    print(searchText);
    List<Todo> filteredList = new List<Todo>();
    for (int i = 0; i < todos.length; i++) {
      if (todos[i].title.toLowerCase().contains(searchText.toLowerCase())) {
        filteredList.add(todos[i]);
      }
    }
    return filteredList;
  }

  List<Todo> sortTodos(List<Todo> list) {
    List<Todo> trueList = list.where((l) => l.isDone == true).toList();
    List<Todo> falseList = list.where((l) => l.isDone == false).toList();
    trueList.sort((a, b) {
      return a.title.toLowerCase().compareTo(b.title.toLowerCase());
    });
    falseList.sort((a, b) {
      return a.title.toLowerCase().compareTo(b.title.toLowerCase());
    });
    List<Todo> resultList = new List.from(falseList)..addAll(trueList);
    return resultList;
  }
}
