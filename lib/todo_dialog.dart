import 'package:flutter/material.dart';

import 'package:todo/todo.dart';

class NewTodoDialog extends StatelessWidget {
  final controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('New task'),
      content: TextField(
        controller: controller,
        autofocus: true,
      ),
      actions: <Widget>[
        FlatButton(
          child: Text('Cancel'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        FlatButton(
          child: Text('Add'),
          onPressed: () {
            String taskText = controller.value.text;
            if (taskText.length > 0) {
              final todo = new Todo(title: controller.value.text);
              controller.clear();
              Navigator.of(context).pop(todo);
            }
            controller.clear();
          },
        ),
      ],
    );
  }
}
